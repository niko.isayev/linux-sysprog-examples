#include <unistd.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 

int main(void) {
    int i;
    int *buff;
    
    buff=(int *) malloc(20*sizeof(int));
    if (buff == NULL)
        return 1;

    for (i=0;i<20;i++)
        buff[i] = i;

    memset(buff, 0, 10*sizeof(int));
    for (i=0;i<20;i++)
        printf("address: %u, value: %d\n", buff+i, *(buff+i));

    free(buff);
    return 0;
}
