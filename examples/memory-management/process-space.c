#include <unistd.h> // execlp()
#include <stdio.h>  // perror()
#include <stdlib.h> // EXIT_SUCCESS, EXIT_FAILURE

static int i = 10; // allocated on Data segment
const char *str = "alo"; // allocated on Data segment
static int j; //allocated on BSS segment

int fun()
{
  char *p_buff; // <-- pointer is allocated on the stack
  int x = 2; // allocated on the stack

  if(x) {
    char buffer[500]; // create 500 bytes on the stack
    p_buff = malloc(sizeof(int)*1000); // create 500 bytes on the heap
  } // <-- buffer is deallocated here, p_buff is not

  return 0;
} // <-- oops there's a memory leak, I should have called free() p_buff;


int main(void) {
  fun();
  exit(EXIT_FAILURE);
}

