// cc -o mq-producer mq-producer.c -lrt -lpthread 

#include <stdio.h>
#include <stdlib.h>
#include <mqueue.h>

#define MQ_NAME "/mq_test"
#define MAX_MSG_SIZE 4096
#define MAX_MSGS     1024

void err(char *msg) {
    perror(msg);
    exit(1);
}

int main( void )
{
    // define queue attributes
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_MSGS;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;
 
	// create a queue
	mqd_t mqd = mq_open(MQ_NAME, O_CREAT | O_WRONLY, 0660, &attr);
    if (mqd == -1) err("mq_open");

    // queue messages
 	mq_send(mqd, "WORLD", 6, 10);
 	mq_send(mqd, "HELLO", 6, 11);

	// cleanup
	mq_close(mqd);
}
