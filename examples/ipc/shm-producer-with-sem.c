/** cc -o shm-producer-with-sem shm-producer-with-sem.c -lrt -lpthread **/
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>

#define SHM_NAME "/shm-test2"
#define SEM_NAME "sem-test2"
#define MSG_SIZE 1024
#define MSG "ola\n"

void err(const char* msg) {
  perror(msg);
  exit(-1);
}

int main() {
  int fd = shm_open(SHM_NAME, O_RDWR | O_CREAT, 0666);
  if (fd < 0) err("Can't open shared mem segment...");

  ftruncate(fd, MSG_SIZE); /* get the bytes */

  void *memptr = mmap(NULL,       /* let system pick where to put segment */
                        MSG_SIZE,   /* how many bytes */
                        PROT_READ | PROT_WRITE, /* access protections */
                        MAP_SHARED, /* mapping visible to other processes */
                        fd,         /* file descriptor */
                        0);         /* offset: start at 1st byte */
  if (!memptr) err("Can't get segment...");

  printf("shared mem address: %p [0..%d]\n", memptr, MSG_SIZE - 1);
  printf("backing file:       /dev/shm%s\n", SHM_NAME );

  /* semaphore code to lock the shared mem */
  sem_t *semptr = sem_open(SEM_NAME,    /* name */
                           O_CREAT,     /* create the semaphore */
                           0666,        /* protection perms */
                           0);          /* initial value */
  if (semptr == (void*) -1) err("sem_open");

  printf("Sleeping 10s before write\n");
  sleep(10); /* give reader a chance */

  printf("Writing msg into shm\n");
  strcpy(memptr, MSG); /* copy some ASCII bytes to the segment */


  /* increment the semaphore so that memreader can read */
  if (sem_post(semptr) < 0) err("sem_post");

  sleep(5); /* give reader a chance */

  /* clean up */
  munmap(memptr, MSG_SIZE); /* unmap the storage */
  close(fd);
  sem_close(semptr);
  sem_unlink(SEM_NAME);
  shm_unlink(SHM_NAME); /* unlink from the backing file */
  return 0;
}
