#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>


void* create_shared_memory(size_t size) 
{
  // Our memory buffer will be readable and writable:
  int protection = PROT_READ | PROT_WRITE;

  // The buffer will be shared (meaning other processes can access it), but
  // anonymous (meaning third-party processes cannot obtain an address for it),
  // so only this process and its children will be able to use it:
  int visibility = MAP_SHARED | MAP_ANONYMOUS;

  // The remaining parameters to `mmap()` are not important for this use case,
  // but the manpage for `mmap` explains their purpose.
  return mmap(NULL, size, protection, visibility, -1, 0);
}


int main(void) 
{
    char parent_message[] = "hello child";  // parent process will write this message
    char child_message[] = "hello parent"; // child process will read then write this one
     
    void* shmem = create_shared_memory(128);
    strncpy(shmem, parent_message, sizeof(parent_message));

    int pid = fork();

    if (pid == 0) {
        printf("[%d] Child read: %s\n", getpid(), shmem);
        strncpy(shmem, child_message, sizeof(child_message));
        printf("[%d] Child wrote: %s\n", getpid(), shmem);
    } else {
        printf("[%d] Parent will sleep 1s:\n", getpid());
        sleep(1);
        printf("[%d] Parent read: %s\n", getpid(), shmem);
    }
    getchar();
    return(0);
}

