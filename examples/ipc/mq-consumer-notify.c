// cc -o mq-consumer-notify mq-consumer-notify.c -lrt

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mqueue.h>
#include <signal.h>

#define MQ_NAME "/mq_test"
#define MAX_MSG_SIZE 4096
#define MAX_MSGS     2

void err(char *msg) {
    perror(msg);
    exit(1);
}

static void tfunc(union sigval sv)
{
    ssize_t recv;
    void *buf;
    mqd_t mqd = *((mqd_t *) sv.sival_ptr);

	char *buffer = calloc(MAX_MSG_SIZE, 1);
    if(!buffer)
        err("Calloc");
    
    int priority = 0;
    while ((recv = mq_receive (mqd, buffer, MAX_MSG_SIZE, &priority)))
      printf("Received [priority %u]: '%s'\n", priority, buffer);
    if(recv == -1) err("Failed to receive message\n");

	// cleanup
	free(buffer);
    exit(EXIT_SUCCESS);
}


int main(void)
{
    mqd_t mqd;
    struct sigevent not;

	mqd = mq_open(MQ_NAME, O_RDONLY);
    if (mqd == -1) err("mq_open");

    not.sigev_notify = SIGEV_THREAD;
    not.sigev_notify_function = tfunc;
    not.sigev_notify_attributes = NULL;
    not.sigev_value.sival_ptr = &mqd;   /* Arg. to thread func. */
    if (mq_notify(mqd, &not) == -1) err("mq_notify");

    sleep(1000);
}
