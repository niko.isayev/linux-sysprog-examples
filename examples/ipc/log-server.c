// cc -o mq-consumer mq-consumer.c -lrt -lpthread 

#include <stdio.h>
#include <stdlib.h>
#include <mqueue.h>
#include <string.h>
#include <unistd.h>




#define MQ_NAME "/log-queue"
#define MAX_MSG_SIZE 4096
#define MAX_MSGS     10
#define LOG_FILE "/tmp/log"

void err(char *msg) {
    perror(msg);
    exit(1);
}

int main( void )
{
    int i, log_fd;

    // open log file
    log_fd =  open(LOG_FILE, O_CREAT|O_WRONLY, S_IRUSR | S_IWUSR);

	// open a message queue for reading
	mqd_t mqd = mq_open(MQ_NAME,O_CREAT | O_RDONLY);
    if (mqd == -1) err("mq_open");

    // setup temp. buffer
	char *buffer = calloc(MAX_MSG_SIZE, 1);
    if(!buffer)
        err("Calloc");

    //retrieve messages from message the queue
    while (1) {
        int priority = 0;
        if ((mq_receive (mqd, buffer, MAX_MSG_SIZE, &priority)) == -1)
          err("Failed to receive message\n");
        else {
          printf("Received [priority %u]: '%s'\n", priority, buffer);
          write(log_fd, buffer, strlen(buffer));
          memset(buffer, 0, MAX_MSG_SIZE);
        }
    }

	// cleanup
	free(buffer);
	mq_close(mqd);
}
