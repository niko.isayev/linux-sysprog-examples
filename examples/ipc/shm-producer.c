#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
 
int main()
{
    /* the size of shared memory object */
    const int SIZE = 4096;
 
    /* name of the shared memory object */
    const char* name = "shm-test";
 
    /* strings written to shared memory */
    const char* msg_0 = "Hello";
    const char* msg_1 = "World!";
 
    /* shared memory file descriptor */
    int shm_fd;
 
    /* pointer to shared memory object */
    void* ptr;
 
    /* create the shared memory object */
    shm_fd = shm_open(name, O_CREAT | O_RDWR, 0666);
 
    /* configure the size of the shared memory object */
    ftruncate(shm_fd, SIZE);
 
    /* memory map the shared memory object */
    ptr = mmap(0, SIZE, PROT_WRITE, MAP_SHARED, shm_fd, 0);
    printf("@%lu\n", ptr);
 
    /* write to the shared memory object */
    sprintf(ptr, "%s", msg_0);
    ptr += strlen(msg_0);
    sprintf(ptr, "%s", msg_1);
    ptr += strlen(msg_1);
    
    return 0;
}
