#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1024

int main(void) {
    int n;
    int fd[2];
    pid_t pid;
    char line[MAXLINE];

    if (pipe(fd) < 0)
        perror("pipe error");
    
    if ((pid = fork()) < 0) {
        perror("fork error");
    } else if (pid > 0) { /* parent */
        /* close read end of pipe*/
        close(fd[0]);
        /* send a message to child */
        write(fd[1], "hello world\n", 12);
    } else { /* child */
        /* close write end of pipe*/
        close(fd[1]);
        /* read message from pipe and print */
        n = read(fd[0], line, MAXLINE);
        write(STDOUT_FILENO, line, n);
    }
    exit(EXIT_SUCCESS);
}

