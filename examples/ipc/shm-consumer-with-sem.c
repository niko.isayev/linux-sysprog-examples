/** cc -o shm-consumer-with-sem shm-consumer-with-sem.c -lrt -lpthread **/
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>

#define SHM_NAME "/shm-test2"
#define SEM_NAME "sem-test2"
#define MSG_SIZE 1024
#define MSG "ola\n"

void err(const char* msg) {
  perror(msg);
  exit(-1);
}

int main() {
  int fd = shm_open(SHM_NAME, O_RDWR, 0666);  /* empty to begin */
  if (fd < 0) err("Can't get file descriptor...");

  /* get a pointer to memory */
  void *memptr = mmap(NULL,       /* let system pick where to put segment */
                        MSG_SIZE,   /* how many bytes */
                        PROT_READ | PROT_WRITE, /* access protections */
                        MAP_SHARED, /* mapping visible to other processes */
                        fd,         /* file descriptor */
                        0);         /* offset: start at 1st byte */
  if (!memptr) err("Can't access segment...");

  /* create a semaphore for mutual exclusion */
  sem_t *semptr = sem_open(SEM_NAME,      /* name */
                           O_CREAT,       /* create the semaphore */
                           0666,          /* protection perms */
                           0);            /* initial value */
  if (semptr == (void*) -1) err("sem_open");

  printf("Waiting for producer to write into shm\n");
  /* use semaphore as a mutex (lock) by waiting for writer to increment it */
  if (!sem_wait(semptr)) { /* wait until semaphore != 0 */
    int i;
    for (i = 0; i < strlen(MSG); i++)
      write(STDOUT_FILENO, memptr + i, 1); /* one byte at a time */
    sem_post(semptr);
  }

  /* cleanup */
  munmap(memptr, MSG_SIZE);
  close(fd);
  sem_close(semptr);
  unlink(SHM_NAME);
  return 0;
}
