// cc -o mq-producer mq-producer.c -lrt -lpthread 

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <mqueue.h>
#include <string.h>
#include <time.h>


#define MQ_NAME "/log-queue"
#define MAX_MSG_SIZE 4096
#define MAX_MSGS     10

void err(char *msg) {
    perror(msg);
    exit(1);
}

char *get_time() {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    return strtok(asctime(tm), "\n");
}

int main(int *argc, char **argv)
{
    pid_t pid; 
    char msg[MAX_MSG_SIZE+sizeof(pid)];

    sprintf(msg, "%s\t%s\t%d\n", get_time(), argv[1], getpid());

    // define queue attributes
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_MSGS;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;
 
	// create a queue
	mqd_t mqd = mq_open(MQ_NAME, O_CREAT | O_WRONLY, 0660, &attr);
    if (mqd == -1) err("mq_open");

    // queue messages
 	mq_send(mqd, msg, strlen(msg), 10);

	// cleanup
	mq_close(mqd);
}
