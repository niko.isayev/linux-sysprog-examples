// cc -o mq-consumer mq-consumer.c -lrt -lpthread 

#include <stdio.h>
#include <stdlib.h>
#include <mqueue.h>



#define MQ_NAME "/mq_test"
#define MAX_MSG_SIZE 4096
#define MAX_MSGS     2

void err(char *msg) {
    perror(msg);
    exit(1);
}

int main( void )
{
    int i;

	// open a message queue for reading
	mqd_t mqd = mq_open(MQ_NAME, O_RDONLY);
    if (mqd == -1) err("mq_open");

    // setup temp. buffer
	char *buffer = calloc(MAX_MSG_SIZE, 1);
    if(!buffer)
        err("Calloc");

    //retrieve messages from message the queue
    while (1) {
        int priority = 0;
        if ((mq_receive (mqd, buffer, MAX_MSG_SIZE, &priority)) == -1)
          err("Failed to receive message\n");
        else
          printf("Received [priority %u]: '%s'\n", priority, buffer);
    }

	// cleanup
	free(buffer);
	mq_close(mqd);
}
