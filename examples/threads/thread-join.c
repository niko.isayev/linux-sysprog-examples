#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


void *start_routine(void *arg)
{
    pthread_exit("ola");
}

int main()
{
    pthread_t tid;
    void *val;

    pthread_create(&tid, NULL, start_routine, NULL);
    // Attempt to join a child thread and get its exit value
    pthread_join(tid, &val);
    printf("thread id:%ld returned: %s\n", tid, (char *)val);

    return 0;
}
