#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>


void *t_func(void *arg)
{
    pthread_t my_thread_id = pthread_self();
    printf("starting thread id: %ld\n", my_thread_id);
    usleep(5000000);
    printf("stopping thread id: %ld\n", my_thread_id);
}


int main()
{
    pthread_t tid1, tid2;
    // start both threads
    pthread_create(&tid1, NULL, t_func, NULL);
    pthread_create(&tid2, NULL, t_func, NULL);
    // sleep for 1s and kill thread 2
    usleep(1000000);
    pthread_cancel(tid2);
    // sleep for 5s and exit
    usleep(10000000);
    return 0;
}
