#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>


void *start_routine(void *arg)
{
    printf("%s\n", (char *) arg);
}

int main()
{
    pthread_t tid1, tid2;
    char *msg1 = "hello from first thread";
    char *msg2 = "hello from second thread";

    pthread_create(&tid1, NULL, start_routine, (void *) msg1);
    pthread_create(&tid2, NULL, start_routine, (void *) msg2);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    return 0;
}
