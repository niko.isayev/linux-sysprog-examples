#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/sysinfo.h>

int counter;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

void *do_work(void *argc)
{
    pthread_mutex_lock(&lock);
    
    unsigned long i = 0;
    counter += 1;
    printf("\n thread %d started\n", counter);
    for(i=0; i<(UINT_MAX);i++);
    printf("\n thread %d finished\n", counter);
    
    pthread_mutex_unlock(&lock);

    return NULL;
}

int main(void)
{
    int i, err, num_threads;
    num_threads = 2;
    pthread_t tid[num_threads];

    for (i=0; i<num_threads; i++) {
        err = pthread_create(&(tid[i]), NULL, &do_work, NULL);
        if (err != 0)
            perror("can't create thread");
    }

    for (i=0; i<num_threads; i++)
        pthread_join(tid[i], NULL);

    pthread_mutex_destroy(&lock);
    
    return 0;
}
