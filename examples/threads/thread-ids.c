#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>


void *t_func(void *arg)
{
    pthread_t my_thread_id = pthread_self();
    pid_t my_pid = getpid();
    pid_t my_tid = syscall(SYS_gettid);

    /* The thread ID returned by this call is not the same thing as a POSIX thread ID 
     * (value returned by pthread_self())
     */
    printf("hello from thread id: %ld, pid: %d, tid: %d\n", my_thread_id, my_pid, my_tid);
}


int main()
{
    pthread_t tid1, tid2;
    pthread_create(&tid1, NULL, t_func, NULL);
    pthread_create(&tid2, NULL, t_func, NULL);
    usleep(1000);
    return 0;
}
