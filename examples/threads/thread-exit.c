#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>


void *t_func1(void *arg)
{
    pthread_t my_thread_id = pthread_self();
    printf("starting thread id: %ld\n", my_thread_id);
    usleep(1000);
    printf("stopping thread id: %ld\n", my_thread_id);
}

void *t_func2(void *arg)
{
    int ret = 123;
    pthread_t my_thread_id = pthread_self();
    printf("starting thread id: %ld\n", my_thread_id);
    usleep(1000);
    pthread_exit(&ret);
    printf("stopping thread id: %ld\n", my_thread_id);
}

int main()
{
    pthread_t tid1, tid2;
    pthread_create(&tid1, NULL, t_func1, NULL);
    pthread_create(&tid2, NULL, t_func2, NULL);
    usleep(2000);
    return 0;
}
