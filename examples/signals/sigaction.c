#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


void err(char *msg) 
{ 
    perror(msg); 
    exit(1); 
}

void signal_handler(int sig)
{
    printf("Got SIGINT\n");
}

int main(void)
{
    int j;
    struct sigaction sa;

    /* setup signal action */
    sa.sa_handler = signal_handler;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGINT, &sa, NULL) < 0)
        err("sigaction error");

    /* Loop continuously waiting for signals to be delivered */
    for (j = 0; ; j++) {
        printf("%d\n", j);
        sleep(1);                     
    }
}
