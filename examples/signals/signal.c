#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


void err(char *msg) 
{ 
    perror(msg); 
    exit(1); 
}

void signal_handler(int sig)
{
    printf("Got SIGINT\n");
}

int main(void)
{
    int j;
    
    if (signal(SIGINT, &signal_handler) == SIG_ERR)
        err("signal");

    /* Loop continuously waiting for signals to be delivered */
    for (j = 0; ; j++) {
        printf("%d\n", j);
        sleep(1);                     
    }
}
