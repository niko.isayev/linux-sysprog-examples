#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


int main(void)
{

    printf("[%d] Pausing until getting signal\n", getpid());
    pause();
}
