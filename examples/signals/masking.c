#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>


void err(char *msg)
{
    perror(msg);
    exit(1);
}

void handler(int sig)
{
    printf("Got signal:%d \n", sig);
}

void print_signal_set(const sigset_t *sigset)
{
    int sig;
    for (sig = 1; sig < NSIG; sig++) {
        if (sigismember(sigset, sig)) {
            printf("%d (%s)\n", sig, strsignal(sig));
        }
    }
}

int main(int argc, char *argv[])
{
    /* Set up a handler for SIGINT */

    printf("Setting up handler for SIGINT\n");

    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = handler;
    if (sigaction(SIGINT, &sa, NULL) == -1)
        err("sigaction");

    /* Block SIGINT for a while */

    sigset_t blocked;
    sigemptyset(&blocked);
    sigaddset(&blocked, SIGINT);
    if (sigprocmask(SIG_SETMASK, &blocked, NULL) == -1)
        err("sigprocmask");

    printf("BLOCKING SIGINT for %d seconds\n", 5);
    sleep(5);

    /* Display mask of pending signals */

    sigset_t pending;
    if (sigpending(&pending) == -1)
        err("sigpending");
    printf("\nPENDING signals are: \n");
    print_signal_set(&pending);

    sleep(5);

    /* Unblock SIGINT */

    printf("UNBLOCKING SIGINT\n");
    sigemptyset(&blocked);
    if (sigprocmask(SIG_SETMASK, &blocked, NULL) == -1)
        err("sigprocmask");

    exit(EXIT_SUCCESS);
}
