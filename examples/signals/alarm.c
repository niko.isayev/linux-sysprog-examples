#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void alarm_handler (int signum)
{
    printf("10 seconds passed, exiting!\n");
    exit(0);
}

int main(void)
{
    signal(SIGALRM, alarm_handler);
    alarm(10);
    printf("Starting to heat up your cpu\n");
    while(1);
    return 0;
}
