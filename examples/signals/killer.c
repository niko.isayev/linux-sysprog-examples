
    


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


void err(char *msg) 
{ 
    perror(msg); 
    exit(1); 
}

int main(int argc, char *argv[])
{
    int sig;
    pid_t pid;

    if (argc < 3) {
        printf("Usage: %s <signal number> <pid>\n", argv[0]);
        exit(1);
    }
    sig = atoi(argv[1]);
    pid = atoi(argv[2]);

    /* Send signals to receiver */
    printf("%s: sending signal %d to process %ld \n", argv[0], sig, (long) pid);
    if(kill(pid, sig) == -1)
        err("kill");

    printf("%s: exiting\n", argv[0]);
    exit(EXIT_SUCCESS);
}
