#include <stdio.h>  // perror()
#include <stdlib.h> // EXIT_SUCCESS, EXIT_FAILURE
#include <sys/time.h> // getrlimit
#include <sys/resource.h> // getrlimit

int main(void) {
    struct rlimit rlim;
    int ret;
    /* get the limit on core sizes */
    ret = getrlimit (RLIMIT_CORE, &rlim);
    if (ret == -1) {
        perror ("getrlimit");
        return 1;
    }
    printf ("RLIMIT_CORE limits: soft=%ld hard=%ld\n", rlim.rlim_cur, rlim.rlim_max);
    exit(EXIT_SUCCESS);
}

