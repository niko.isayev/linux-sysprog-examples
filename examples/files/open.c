#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


int main(void) {
    int fd;
    fd = open("/etc/passwd", O_RDONLY | O_SYNC);
    if (fd < 0)
        perror("open failed");
    close(fd);

    return 0;
}

