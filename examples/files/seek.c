#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>


int main() {
    int fd;
    char *buf = "dddddd";
    fd = open("/tmp/seek", O_CREAT|O_WRONLY, S_IRUSR | S_IWUSR);
    if (fd < 0)
        perror("open failed");
    printf("current pos: %d\n", lseek(fd, 0, SEEK_CUR));
    printf("writing to file\n");
    write(fd, buf, strlen(buf));
    printf("current pos: %d\n", lseek(fd, 0, SEEK_CUR));
    lseek(fd, 30000000, SEEK_END);
    printf("current pos: %d\n", lseek(fd, 0, SEEK_CUR));
    close(fd);
    return 0;
}

