#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define FN "/tmp/hole"

int main() {
    creat(FN, O_TRUNC | S_IRUSR | S_IWUSR);
    if (truncate(FN, 30000) < 0)
        perror("truncate failed");
    return 0;
}

