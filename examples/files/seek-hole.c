#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>


int main() {
    int fd;
    char *buff = "ddd";
    fd = open("/tmp/file.hole", O_CREAT|O_WRONLY, S_IRUSR | S_IWUSR);
    if (fd < 0)
        perror("open failed");
    write(fd, buff, strlen(buff));
    lseek(fd, 3000000, SEEK_SET);
    close(fd);
    return 0;
}

