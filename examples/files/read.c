#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define BUFFSIZE 1024000


int main(void) {
    int fd, nread;
    char buffer[BUFFSIZE];
    fd = open("/etc/passwd", O_RDONLY | O_SYNC);
    if (fd < 0)
        perror("open failed");
    read(fd, buffer, BUFFSIZE);
    fputs(buffer, stdout);
    close(fd);
    return 0;
}

