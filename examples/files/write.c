#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define WR_FILE "/tmp/xxx"


int main()
{
    int n, fd;
    const char *buf = "yyy\n";

    fd = open(WR_FILE, O_CREAT|O_WRONLY, S_IRUSR | S_IWUSR);
    
    if (write(fd, buf, strlen(buf)) < 0)
        perror("write error");

    
    return 0;
}
